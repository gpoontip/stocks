import axios from "axios";
import { Position } from "src/types/db";

const createPosition = (data: Position) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/positions`, data);
};

const getAllPositions = async () => {
  return axios.get(`${process.env.REACT_APP_API_URL}/positions`);
};

const getPosition = async (id: string) => {
  return axios.get(`${process.env.REACT_APP_API_URL}/positions/${id}`);
};

const updatePosition = (id: string, data: Position) => {
  return axios.patch(`${process.env.REACT_APP_API_URL}/positions/${id}`, data);
};

const deletePosition = (id: string) => {
  return axios.delete(`${process.env.REACT_APP_API_URL}/positions/${id}`);
};

export {
  createPosition,
  getAllPositions,
  getPosition,
  updatePosition,
  deletePosition,
};
