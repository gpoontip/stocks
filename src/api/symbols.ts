import axios from "axios";
import { Symbol } from "src/types/db";

const createSymbol = (id: string, data: Symbol) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/symbols/${id}`, data);
};

const getAllSymbols = async () => {
  return axios.get(`${process.env.REACT_APP_API_URL}/symbols`);
};

const getSymbol = async (id: string) => {
  return axios.get(`${process.env.REACT_APP_API_URL}/symbols/${id}`);
};

const updateSymbol = (id: string, data: Symbol) => {
  return axios.patch(`${process.env.REACT_APP_API_URL}/symbols/${id}`, data);
};

const deleteSymbol = (id: string) => {
  return axios.delete(`${process.env.REACT_APP_API_URL}/symbols/${id}`);
};

export { createSymbol, getAllSymbols, getSymbol, updateSymbol, deleteSymbol };
