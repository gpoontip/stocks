import axios from "axios";

const fhStockQuote = (symbol: string) => {
  return axios.get(
    `${process.env.REACT_APP_FINNHUB_URL}/quote?symbol=${symbol}&token=${process.env.REACT_APP_FINNHUB_KEY}`
  );
};

const fhStockCompany = (symbol: string) => {
  return axios.get(
    `${process.env.REACT_APP_FINNHUB_URL}/stock/profile2?symbol=${symbol}&token=${process.env.REACT_APP_FINNHUB_KEY}`
  );
};

export { fhStockQuote };
