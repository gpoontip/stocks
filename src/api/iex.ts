import axios from "axios";

const iexStockSearch = (symbol: string) => {
  return axios.get(
    `${process.env.REACT_APP_IEX_URL}/search/${symbol}?token=${process.env.REACT_APP_IEX_KEY}`
  );
};

const iexStockCompany = (symbol: string) => {
  return axios.get(
    `${process.env.REACT_APP_IEX_URL}/stock/${symbol}/company?token=${process.env.REACT_APP_IEX_KEY}`
  );
};

const iexStockQuote = (symbol: string) => {
  return axios.get(
    `${process.env.REACT_APP_IEX_URL}/stock/${symbol}/quote?token=${process.env.REACT_APP_IEX_KEY}`
  );
};

const iexBatchQuote = (symbols: string) => {
  return axios.get(
    `${process.env.REACT_APP_IEX_URL}/stock/market/batch?symbols=${symbols}&types=quote&token=${process.env.REACT_APP_IEX_KEY}`
  );
};

export { iexStockSearch, iexStockCompany, iexStockQuote, iexBatchQuote };
