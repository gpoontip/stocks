import { createPosition } from "src/api/positions";
import { createSymbol } from "src/api/symbols";
import { iexStockCompany, iexStockQuote } from "src/api/iex";
// types
import { IexStockCompany, IexStockQuote } from "src/types/iex";

const createPositionHelper = async (symbol: string) => {
  try {
    const response = await Promise.all([
      iexStockCompany(symbol),
      iexStockQuote(symbol),
    ]);

    const company: IexStockCompany = response[0].data;
    const quote: IexStockQuote = response[1].data;

    return Promise.all([
      // create position
      createPosition({
        symbol: symbol.toUpperCase(),
        company,
        quote,
        initialPrice: quote.latestPrice,
        status: "watching",
      }),

      // create symbol
      createSymbol(symbol.toUpperCase(), { company, quote }),
    ]);
  } catch (error) {
    return Promise.reject(error);
  }
};

const percent = (number: number) => {
  return (number * 100).toFixed(2);
};

const color = (number: number) => {
  if (number > 0) return "green";
  else if (number < 0) return "volcano";
  return "default";
};

export { createPositionHelper, percent, color };
