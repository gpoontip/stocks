// types
import { Position } from "src/types/app";
import { IexBatchQuote } from "src/types/iex";
// api
import { updatePosition } from "src/api/positions";
import { updateSymbol } from "src/api/symbols";
import { iexBatchQuote } from "src/api/iex";

const profitLoss = (sellPrice: number, costPrice: number) => {
  const gain = sellPrice - costPrice;
  return gain / costPrice;
};

const calculateChange = (position: Position) => {
  let price = 0;
  if (position.status === "watching") price = position.initialPrice;
  else if (position.status === "active") price = position.buyPrice;
  else if (position.status === "closed") price = position.sellPrice;
  return profitLoss(position.quote.latestPrice, price);
};

const timeout = (ms: number) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

// 1. get latest iex prices
// 2. update symbols db
// 3. update each position
const getLatestPrices = (positions: Position[]) => {
  return new Promise(async (resolve, reject) => {
    // loop through unique positions
    const symbols = positions
      .map((row) => row.symbol)
      .filter((row, index, array) => array.indexOf(row) === index);

    // batch endpoint limited to 100 symbols
    if (symbols.length) {
      let symbolsString = "";
      const symbolBatches = [];
      // loop through symbols
      for (let i = 0; i < symbols.length; i++) {
        symbolsString += symbols[i] + ",";
        const count = i + 1;
        // break up symbols into batches of 100
        if (count % 100 === 0) {
          symbolBatches.push(symbolsString.slice(0, -1));
          symbolsString = "";
        }
      }
      symbolBatches.push(symbolsString.slice(0, -1));

      // submit batch requests to IEX with 100ms intervals
      let entries: [string, IexBatchQuote][] = [];
      for (const batch of symbolBatches) {
        const response = await iexBatchQuote(batch);
        const data: IexBatchQuote[] = response.data;
        const entry = Object.entries(data);
        entries = [...entries, ...entry];
        await timeout(100);
      }

      await Promise.all([
        Promise.all(
          entries.map(([symbol, { quote }]) => updateSymbol(symbol, { quote }))
        ),
        Promise.all(
          entries.map(async ([symbol, { quote }]) => {
            // get all positions to update
            const update = positions.filter((row) => row.symbol === symbol);
            // save each position
            return await Promise.all(
              update.map((row) => {
                const profitLoss = calculateChange({ ...row, quote });
                return updatePosition(row.id, { quote, profitLoss });
              })
            );
          })
        ),
      ]);
    }
    resolve("ok");
  });
};

export { getLatestPrices, profitLoss };
