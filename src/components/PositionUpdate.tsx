import React, { useEffect } from "react";
import { Input, Modal } from "antd";

type Props = {
  title: string;
  populateWith: string;
  visible: boolean;
  onOk: (price: string) => void;
  onCancel: () => void;
};

const PositionUpdate = ({
  title,
  populateWith,
  visible,
  onOk,
  onCancel,
}: Props) => {
  const [price, setPrice] = React.useState("");

  useEffect(() => {
    setPrice(populateWith);
  }, [populateWith]);

  return (
    <Modal
      title={title}
      visible={visible}
      onOk={() => onOk(price)}
      onCancel={onCancel}
      okText="Submit"
    >
      <Input
        addonBefore="$"
        placeholder="Price"
        value={price}
        onChange={(e) => setPrice(e.target.value)}
      />
    </Modal>
  );
};

export default PositionUpdate;
