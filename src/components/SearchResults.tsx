import React from "react";
import { IexStockSearch } from "src/types/iex";
import { Button } from "antd";

type Props = {
  results: IexStockSearch[];
  onClick: (e: React.MouseEvent, symbol: string) => void;
};

const SearchResults = ({ results, onClick }: Props) => {
  return (
    <div>
      {results.map(({ symbol }) => (
        <p key={symbol}>
          <Button type="link" onClick={(e) => onClick(e, symbol)}>
            {symbol}
          </Button>
        </p>
      ))}
    </div>
  );
};

export default SearchResults;
