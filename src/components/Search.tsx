import React from "react";
import { Input } from "antd";

type Props = {
  onSubmit: (symbol: string) => void;
};

const { Search } = Input;

const StocksAdd = ({ onSubmit }: Props) => {
  const [symbol, setSymbol] = React.useState("");

  return (
    <Search
      placeholder="Symbol"
      onSearch={(symbol) => {
        setSymbol("");
        onSubmit(symbol);
      }}
      onChange={(e) => setSymbol(e.target.value)}
      value={symbol}
      enterButton
      style={{ width: 200 }}
    />
  );
};

export default StocksAdd;
