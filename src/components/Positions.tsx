import { Position } from "src/types/app";
import { Button, Table, Space, Tag } from "antd";
import { percent, color } from "src/helpers/positions";
import PositionStats from "src/components/PositionStats";

type Props = {
  positions: Position[];
  onBuyClick: (id: string, price: number) => void;
  onSellClick: (id: string, price: number) => void;
  onDeleteClick: (id: string) => void;
};
declare type SortOrder = "descend" | "ascend" | null;

const Positions = ({
  positions,
  onBuyClick,
  onSellClick,
  onDeleteClick,
}: Props) => {
  const columns = [
    {
      title: "Symbol",
      dataIndex: "symbol",
      sorter: (a: Position, b: Position) => {
        if (a.symbol < b.symbol) {
          return -1;
        }
        if (a.symbol > b.symbol) {
          return 1;
        }
        // names must be equal
        return 0;
      },
    },
    {
      title: "Price",
      key: "latestPrice",
      render: (row: Position) => <div>{row.quote.latestPrice}</div>,
      sorter: (a: Position, b: Position) =>
        a.quote.latestPrice - b.quote.latestPrice,
    },
    {
      title: "Change",
      key: "changePercent",
      render: (row: Position) => (
        <Tag color={color(row.quote.changePercent)}>
          {percent(row.quote.changePercent)}%
        </Tag>
      ),
      sorter: (a: Position, b: Position) =>
        a.quote.changePercent - b.quote.changePercent,
    },
    {
      title: "P/L",
      dataIndex: "profitLoss",
      render: (change: number) => {
        return <Tag color={color(change)}>{percent(change)}%</Tag>;
      },
      defaultSortOrder: "descend" as SortOrder,
      sorter: (a: Position, b: Position) => {
        if (a.profitLoss && b.profitLoss) return a.profitLoss - b.profitLoss;
        return 0;
      },
    },
    {
      title: "Action",
      key: "action",
      render: (row: Position) => (
        <Space style={{ width: "135px" }}>
          {row.status === "watching" && (
            <Button
              type="link"
              onClick={() => onBuyClick(row.id, row.quote.latestPrice)}
            >
              Buy
            </Button>
          )}
          {row.status === "active" && (
            <Button
              type="link"
              onClick={() => onSellClick(row.id, row.quote.latestPrice)}
            >
              Sell
            </Button>
          )}
          <Button type="link" onClick={() => onDeleteClick(row.id)}>
            Delete
          </Button>
        </Space>
      ),
    },
  ];

  const active = positions.filter((row) => row.status === "active");
  const watching = positions.filter((row) => row.status === "watching");
  const closed = positions.filter((row) => row.status === "closed");

  return (
    <div>
      <Space align="center">
        <h2>Active Positions</h2>
        <PositionStats data={active} />
      </Space>
      <Table
        rowKey="id"
        columns={columns}
        dataSource={active}
        size="small"
        scroll={{ x: true }}
      />
      <Space align="center">
        <h2>Watchlist</h2>
        <PositionStats data={watching} />
      </Space>
      <Table
        rowKey="id"
        columns={columns}
        dataSource={watching}
        size="small"
        scroll={{ x: true }}
      />
      <Space align="center">
        <h2>Closed Positions</h2>
        <PositionStats data={closed} />
      </Space>
      <Table
        rowKey="id"
        columns={columns}
        dataSource={closed}
        size="small"
        scroll={{ x: true }}
      />
    </div>
  );
};

export default Positions;
