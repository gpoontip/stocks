import { Tag } from "antd";
import { Position } from "src/types/app";
import { percent, color } from "src/helpers/positions";

type Props = {
  data: Position[];
};

const PositionStats = ({ data }: Props) => {
  const profitLoss = data.length
    ? data.map((row) => row.profitLoss).reduce((total, num) => total + num)
    : 0;

  const change = data.length
    ? data
        .map((row) => row.quote.changePercent)
        .reduce((total, num) => total + num)
    : 0;

  return (
    <div style={{ marginBottom: "0.25rem" }}>
      Change <Tag color={color(change)}>{percent(change)}%</Tag>
      P/L <Tag color={color(profitLoss)}>{percent(profitLoss)}%</Tag>
    </div>
  );
};

export default PositionStats;
