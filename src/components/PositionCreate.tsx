import React from "react";
import { Form, Input, Button, Modal } from "antd";

type Props = {
  title: string;
  visible: boolean;
  onSubmit: (symbol: string) => Promise<boolean>;
  onCancel: () => void;
};

const PositionCreate = ({ title, visible, onSubmit, onCancel }: Props) => {
  const [form] = Form.useForm();
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };
  const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
  };

  const onFinish = async ({ symbol }: any) => {
    try {
      await onSubmit(symbol);
      form.resetFields();
    } catch (error) {
      console.error(error.message);
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    console.error("Failed:", errorInfo);
  };

  return (
    <Modal title={title} visible={visible} footer={null} onCancel={onCancel}>
      <Form
        form={form}
        {...layout}
        name="basic"
        initialValues={{ symbol: "" }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Symbol"
          name="symbol"
          rules={[{ required: true, message: "Symbol is required" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Add
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default PositionCreate;
