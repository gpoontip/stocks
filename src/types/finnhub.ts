export type FhStockQuote = {
  c: number;
  h: number;
  l: number;
  o: number;
  pc: number;
  t: number;
};
