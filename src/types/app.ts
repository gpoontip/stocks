import { IexStockCompany, IexStockQuote } from "./iex";

export type Position = {
  id: string;
  symbol: string;
  status: string;
  initialPrice: number;
  quote: IexStockQuote;
  company: IexStockCompany;
  buyPrice: number;
  sellPrice: number;
  profitLoss: number;
};
