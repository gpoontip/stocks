import { IexStockCompany, IexStockQuote } from "./iex";

export type Position = {
  symbol?: string;
  status?: string;
  company?: IexStockCompany;
  quote?: IexStockQuote;
  initialPrice?: number;
  sellPrice?: number;
  buyPrice?: number;
  profitLoss?: number;
};

export type Symbol = {
  company?: IexStockCompany;
  quote?: IexStockQuote;
};
