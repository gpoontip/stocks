import React, { Component } from "react";
// antd
import { Modal, Button, Spin, notification, Row, Col } from "antd";
import { ExclamationCircleOutlined, PlusOutlined } from "@ant-design/icons";
import { RedoOutlined } from "@ant-design/icons";
// components
import Positions from "src/components/Positions";
import Search from "src/components/Search";
import SearchResults from "src/components/SearchResults";
import PositionCreate from "src/components/PositionCreate";
import PositionUpdate from "src/components/PositionUpdate";
// api
import {
  updatePosition,
  getAllPositions,
  deletePosition,
} from "src/api/positions";
import { iexStockSearch } from "src/api/iex";
// types
import { Position } from "src/types/app";
import { IexStockSearch } from "src/types/iex";
// helpers
import { getLatestPrices, profitLoss } from "src/helpers/stocks";
import { createPositionHelper } from "src/helpers/positions";

const { confirm } = Modal;

type StocksWatchingState = {
  positions: Position[];
  searchResults: IexStockSearch[];
  isBuyModalVisible: boolean;
  isSellModalVisible: boolean;
  isPositionCreateVisible: boolean;
  positionId: string;
  positionPrice: number;
  loading: boolean;
  refreshing: boolean;
};

class StocksWatch extends Component<{}, StocksWatchingState> {
  constructor(props: any) {
    super(props);
    this.state = {
      positions: [],
      searchResults: [],
      isBuyModalVisible: false,
      isSellModalVisible: false,
      isPositionCreateVisible: false,
      positionId: "",
      positionPrice: 0,
      loading: false,
      refreshing: false,
    };
  }

  componentDidMount() {
    this.getPositions();
  }

  getPositions = async () => {
    this.setState({ loading: true });
    try {
      const response = await getAllPositions();
      this.setState({ positions: response.data });
    } catch (error) {
      console.error(error.message);
    }
    this.setState({ loading: false });
  };

  handleStockSearch = async (symbol: string) => {
    this.setState({ loading: true });
    if (symbol) {
      try {
        const response = await iexStockSearch(symbol);
        this.setState({ searchResults: response.data });
      } catch (error) {
        console.error(error.message);
      }
    } else {
      this.setState({ searchResults: [] });
    }
    this.setState({ loading: false });
  };

  handleStockClick = async (e: React.MouseEvent, symbol: string) => {
    e.preventDefault();
    // show preloader
    this.setState({ loading: true });
    // create position
    await createPositionHelper(symbol);
    // refresh list
    this.getPositions();
    // clear results
    this.setState({ searchResults: [] });
  };

  handleBuy = (positionId: string, positionPrice: number) => {
    this.setState({ isBuyModalVisible: true, positionId, positionPrice });
  };

  handleSubmitBuy = async (price: string) => {
    this.setState({ isBuyModalVisible: false });
    try {
      const buyPrice = parseFloat(price);
      const position = this.state.positions.find(
        (row) => row.id === this.state.positionId
      );
      const latestPrice = position?.quote.latestPrice as number;
      await updatePosition(this.state.positionId, {
        status: "active",
        buyPrice,
        profitLoss: latestPrice ? profitLoss(latestPrice, buyPrice) : 0,
      });
      this.getPositions();
    } catch (error) {
      console.error(error.message);
    }
  };

  handleCancelBuy = () => {
    this.setState({ isBuyModalVisible: false });
  };

  handleSell = (positionId: string, positionPrice: number) => {
    this.setState({ isSellModalVisible: true, positionId, positionPrice });
  };

  handleSubmitSell = async (price: string) => {
    this.setState({ isSellModalVisible: false });
    try {
      const sellPrice = parseFloat(price);
      const position = this.state.positions.find(
        (row) => row.id === this.state.positionId
      );
      const latestPrice = position?.quote.latestPrice as number;
      await updatePosition(this.state.positionId, {
        status: "closed",
        sellPrice,
        profitLoss: latestPrice ? profitLoss(latestPrice, sellPrice) : 0,
      });
      this.getPositions();
    } catch (error) {
      console.error(error.message);
    }
  };

  handleCancelSell = () => {
    this.setState({ isSellModalVisible: false });
  };

  handleDelete = async (positionId: string) => {
    confirm({
      title: "Do you Want to delete this position?",
      icon: <ExclamationCircleOutlined />,
      content: "This is permanent.",
      onOk: async () => {
        try {
          await deletePosition(positionId);
          this.getPositions();
        } catch (error) {
          console.error(error.message);
        }
      },
    });
  };

  fetchPrices = async () => {
    this.setState({ refreshing: true });
    try {
      await getLatestPrices(this.state.positions);
      this.getPositions();
    } catch (error) {
      console.error(error.message);
    }
    this.setState({ refreshing: false });
  };

  showPositionUpdate = () => this.setState({ isPositionCreateVisible: true });

  hidePositionUpdate = () => this.setState({ isPositionCreateVisible: false });

  handlePositionCreate = async (symbol: string) => {
    // show preloader
    this.setState({ loading: true });
    // create position
    try {
      await createPositionHelper(symbol);
      // hide modal
      this.hidePositionUpdate();
    } catch (error) {
      this.setState({ loading: false });
      notification.error({
        message: "Error",
        description: "Could not find symbol",
      });
      return Promise.reject(error);
    }
    // refresh list
    await this.getPositions();
    this.setState({ loading: false });
    return Promise.resolve(true);
  };

  render() {
    return (
      <Spin spinning={this.state.loading}>
        <Row gutter={[8, 8]}>
          <Col>
            <Search onSubmit={(symbol) => this.handleStockSearch(symbol)} />
          </Col>
          <Col>
            <Button
              type="primary"
              icon={<PlusOutlined />}
              onClick={this.showPositionUpdate}
            >
              Add Symbol
            </Button>
          </Col>
          <Col>
            <Button
              type="ghost"
              icon={<RedoOutlined />}
              onClick={this.fetchPrices}
              loading={this.state.refreshing}
              disabled={this.state.refreshing}
            >
              Refresh
            </Button>
          </Col>
        </Row>
        <SearchResults
          results={this.state.searchResults}
          onClick={(e, symbol) => this.handleStockClick(e, symbol)}
        />
        <br />
        <br />
        <Positions
          positions={this.state.positions}
          onBuyClick={(id, price) => this.handleBuy(id, price)}
          onSellClick={(id, price) => this.handleSell(id, price)}
          onDeleteClick={(id) => this.handleDelete(id)}
        />
        <PositionUpdate
          title="Buy Position"
          visible={this.state.isBuyModalVisible}
          onOk={(price) => this.handleSubmitBuy(price)}
          onCancel={this.handleCancelBuy}
          populateWith={this.state.positionPrice.toString()}
        />
        <PositionUpdate
          title="Sell Position"
          visible={this.state.isSellModalVisible}
          onOk={(price) => this.handleSubmitSell(price)}
          onCancel={this.handleCancelSell}
          populateWith={this.state.positionPrice.toString()}
        />
        <PositionCreate
          title="Create Position"
          visible={this.state.isPositionCreateVisible}
          onSubmit={(symbol) => this.handlePositionCreate(symbol)}
          onCancel={this.hidePositionUpdate}
        />
      </Spin>
    );
  }
}
export default StocksWatch;
