import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";
import "firebase/database";

const firebaseConfig = {
  apiKey: "AIzaSyC5RcE_j0Obh6CgOghySTwA0JuOS9-m-uw",
  authDomain: "stocks-456e9.firebaseapp.com",
  projectId: "stocks-456e9",
  storageBucket: "stocks-456e9.appspot.com",
  messagingSenderId: "41139002550",
  appId: "1:41139002550:web:8d645ad73d69d5ea90da17",
  measurementId: "G-2BNK7SBKY2",
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const firestore = firebase.firestore();
export const db = firebase.database();
export const auth = firebase.auth();
export const FieldValue = firebase.firestore.FieldValue;
export function getNewKey(child: string) {
  return db.ref().child(child).push().key;
}
