import "./App.css";
import Stocks from "src/views/Stocks";
import { Layout } from "antd";

const { Content } = Layout;

const App = () => (
  <Layout>
    <Content style={{ padding: "1rem" }}>
      <Stocks />
    </Content>
  </Layout>
);

export default App;
