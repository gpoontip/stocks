import { Response } from "express";
import { firestore } from "./config/firebase";
import { serverTimestamp } from "./helpers/date";

type SymbolType = {
  company: {};
  quote: {};
  comments: string;
};

type Request = {
  body: SymbolType;
  params: { id: string };
};

const createSymbol = async (req: Request, res: Response) => {
  const {
    body: { company, quote },
    params: { id },
  } = req;
  try {
    const symbol = firestore.collection("symbols").doc(id);
    const data = {
      company,
      quote,
      comments: "",
    };

    symbol
      .set({
        ...data,
        created: serverTimestamp(),
        modified: serverTimestamp(),
      })
      .catch((error) =>
        res.status(400).json({
          status: "error",
          message: error.message,
        })
      );

    res.status(200).json(data);
  } catch (error) {
    res.status(500).json(error.message);
  }
};

const updateSymbol = async (req: Request, res: Response) => {
  const {
    body: { company, quote, comments },
    params: { id },
  } = req;

  try {
    const symbol = firestore.collection("symbols").doc(id);
    const currentData = (await symbol.get()).data() || {};
    const data = {
      quote: quote || currentData.quote,
      company: company || currentData.company,
      comments: comments || currentData.comments,
    };

    await symbol
      .set({ ...data, modified: serverTimestamp() }, { merge: true })
      .catch((error) => {
        return res.status(400).json({
          status: "error",
          message: error.message,
        });
      });

    return res.status(200).json(data);
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

const getSymbol = async (req: Request, res: Response) => {
  const {
    params: { id },
  } = req;
  try {
    const doc = await firestore.collection("symbols").doc(id).get();
    const data = doc.data() as SymbolType;
    return res.status(200).json(data);
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

const getAllSymbols = async (req: Request, res: Response) => {
  try {
    const allSymbols = await firestore.collection("symbols").get();
    const data: SymbolType[] = [];
    allSymbols.forEach((doc) => {
      const symbol = doc.data() as SymbolType;
      data.push(symbol);
    });
    return res.status(200).json(data);
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

const deleteSymbol = async (req: Request, res: Response) => {
  const { id } = req.params;

  try {
    const symbol = firestore.collection("symbols").doc(id);

    await symbol.delete().catch((error) => {
      return res.status(400).json({
        status: "error",
        message: error.message,
      });
    });

    return res.status(200).json({
      status: "success",
      message: "symbol deleted successfully",
    });
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

export { createSymbol, getSymbol, getAllSymbols, updateSymbol, deleteSymbol };
