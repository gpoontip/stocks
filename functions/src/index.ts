import * as functions from "firebase-functions";
import * as express from "express";
import {
  createSymbol,
  getSymbol,
  getAllSymbols,
  updateSymbol,
  deleteSymbol,
} from "./symbolController";
import {
  createPosition,
  getPosition,
  getAllPositions,
  updatePosition,
  deletePosition,
} from "./positionController";

const app = express();

const cors = require("cors");
const whitelist = [
  `https://${functions.config().project.id}.firebaseapp.com`,
  `https://${functions.config().project.id}.web.app`,
  "http://localhost:3030",
  "http://localhost:3000",
  "https://web.postman.co",
];
const corsOptions = {
  origin: (origin: any, callback: any) => {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else if (origin) {
      callback(new Error("Not allowed by CORS"));
    } else {
      callback(null, true);
    }
  },
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};
app.use(cors(corsOptions));

app.get("/", (req, res) => res.status(200).send("Hey there!"));

app.post("/symbols/:id", createSymbol);
app.patch("/symbols/:id", updateSymbol);
app.get("/symbols", getAllSymbols);
app.get("/symbols/:id", getSymbol);
app.delete("/symbols/:id", deleteSymbol);

app.post("/positions", createPosition);
app.patch("/positions/:id", updatePosition);
app.get("/positions", getAllPositions);
app.get("/positions/:id", getPosition);
app.delete("/positions/:id", deletePosition);

exports.app = functions.https.onRequest(app);
