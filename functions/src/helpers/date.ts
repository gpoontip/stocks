import { FieldValue, ServerValue } from "../config/firebase";

const timestamp = () => {
  // for rtdb
  return ServerValue.TIMESTAMP;
};

const serverTimestamp = () => {
  // for firestore
  return FieldValue.serverTimestamp();
};

export { timestamp, serverTimestamp };
