import { Response } from "express";
import { db, getNewKey } from "./config/firebase";
import { timestamp } from "./helpers/date";

type PositionType = {
  symbol: string;
  status: string;
  company: {};
  quote: {};
  initialPrice: number;
  buyPrice: number;
  sellPrice: number;
  profitLoss: number;
  quantity: number;
};

type Request = {
  body: PositionType;
  params: { id: string };
};

const createPosition = async (req: Request, res: Response) => {
  const { symbol, status, company, quote, initialPrice } = req.body;
  try {
    const id = getNewKey("positions");
    const position = db.ref(`positions/${id}`);
    const data = {
      id,
      symbol,
      status,
      company: company,
      quote: quote,
      initialPrice: initialPrice,
      buyPrice: 0,
      sellPrice: 0,
      profitLoss: 0,
      quantity: 1,
    };

    position
      .set({ ...data, modified: timestamp(), created: timestamp() })
      .catch((error) =>
        res.status(400).json({
          status: "error",
          message: error.message,
        })
      );

    res.status(200).json(data);
  } catch (error) {
    res.status(500).json(error.message);
  }
};

const updatePosition = async (req: Request, res: Response) => {
  const {
    body: {
      symbol,
      status,
      company,
      quote,
      initialPrice,
      buyPrice,
      sellPrice,
      profitLoss,
      quantity,
    },
    params: { id },
  } = req;

  try {
    const position = db.ref(`positions/${id}`);
    const currentData = (await position.once("value")).val() || {};
    const data = {
      id: currentData.id,
      symbol: symbol || currentData.symbol,
      status: status || currentData.status,
      company: company || currentData.company,
      quote: quote || currentData.quote,
      initialPrice: initialPrice || currentData.initialPrice,
      buyPrice: buyPrice || currentData.buyPrice,
      sellPrice: sellPrice || currentData.sellPrice,
      profitLoss: profitLoss || currentData.profitLoss,
      quantity: quantity || currentData.quantity,
    };

    await position.update({ ...data, modified: timestamp() }).catch((error) => {
      return res.status(400).json({
        status: "error",
        message: error.message,
      });
    });

    return res.status(200).json(data);
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

const getPosition = async (req: Request, res: Response) => {
  const {
    params: { id },
  } = req;

  try {
    const snapshot = await db.ref(`positions/${id}`).once("value");
    const data = snapshot.val() as PositionType;
    return res.status(200).json(data);
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

const getAllPositions = async (req: Request, res: Response) => {
  try {
    const allPositions = await db.ref("positions").once("value");
    const data: PositionType[] = [];
    allPositions.forEach((snapshot) => {
      const position = snapshot.val() as PositionType;
      data.push(position);
    });
    return res.status(200).json(data);
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

const deletePosition = async (req: Request, res: Response) => {
  const { id } = req.params;

  try {
    const position = db.ref(`positions/${id}`);

    await position.remove().catch((error) => {
      return res.status(400).json({
        status: "error",
        message: error.message,
      });
    });

    return res.status(200).json({
      status: "success",
      message: "position deleted successfully",
    });
  } catch (error) {
    return res.status(500).json(error.message);
  }
};

export {
  createPosition,
  getPosition,
  getAllPositions,
  updatePosition,
  deletePosition,
};
